package com.hp.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hsqldb.Server;
import org.hsqldb.persist.HsqlProperties;
import org.hsqldb.server.ServerAcl.AclFormatException;

import com.hp.business.User;
import com.hp.controller.db.DBManager;

/**
 * Servlet implementation class MyController
 */
@WebServlet("/SimpleController")
public class SimpleController extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

    private static final long serialVersionUID = 9213634523623237240L;
    Connection con;
    Server server;

    public SimpleController() {
        super();
    }

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            System.out.println("Starting Database");
            HsqlProperties p = new HsqlProperties();
            p.setProperty("server.database.0", "file:/opt/db/crm");
            p.setProperty("server.dbname.0", "mydb");
            p.setProperty("server.port", "9001");
            server = new Server();
            server.setProperties(p);
            server.setLogWriter(null);
            server.setErrWriter(null);
            server.start();
            System.out.println("database started");

            try {
                Class.forName("org.hsqldb.jdbc.JDBCDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace(System.out);
            }
            try {
                con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost:9001/mydb", "SA", "");
                con.createStatement().executeUpdate(
                        "create table users (name varchar(45),surname varchar(45),email varchar(45))");
                ServletContext context = getServletContext();
                context.setAttribute("con", con);
            } catch (SQLException e) {
                System.out.println("Table users already created");
            }

        } catch (AclFormatException afex) {
            throw new ServletException(afex);
        } catch (IOException ioex) {
            throw new ServletException(ioex);
        }
    }

    /*
     * (non-Java-doc)
     * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<User> users = new ArrayList<User>();

        String type = request.getParameter("Submit");
        if ("View Table".equals(type)) {
            try {
                users = DBManager.getAllUsers(con);
            } catch (SQLException e) {
                System.err.println("Error in doGet method - View Table");
            }
        }

        else {

            String name = request.getParameter("name");
            String surname = request.getParameter("surname");
            String email = request.getParameter("email");
            if (validateEmail(email)) {
                try {
                    User userInDB = DBManager.findUserByEmail(email, con);
                    if (userInDB == null) {
                        DBManager.insertUserInDB(name, surname, email, con);
                    } else {
                        DBManager.updateUserInDB(name, surname, email, con);
                    }
                    users = DBManager.getAllUsers(con);
                } catch (SQLException e) {
                    System.err.println("Error in doGet method");
                }

            } else {
                try {
                    users = DBManager.getAllUsers(con);
                } catch (SQLException e) {
                    System.err.println("Error in doGet method");
                }
            }
        }

        HttpSession session = request.getSession();
        session.setAttribute("users", users);

        RequestDispatcher dispatcher;
        dispatcher = getServletContext().getRequestDispatcher("/ShowUser.jsp");
        dispatcher.forward(request, response);
    }

    public static boolean validateEmail(String email) {
        String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        Boolean b = email.matches(emailreg);

        return b;
    }

    /*
     * (non-Java-doc)
     * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        this.doGet(request, response);
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();

        try {
            con.close();
        } catch (SQLException e) {
            System.err.println("[destroy] error during connection.close()");
        }
    }
}
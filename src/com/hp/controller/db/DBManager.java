package com.hp.controller.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hp.business.User;

public class DBManager {
    
    /**
     * 
     * Method used to insert a new user in database
     * Returns true is the insert operation was successful .
     * otherwise false
     * 
     * @param name
     * @param surname
     * @param email
     * @param con
     * @return
     * @throws SQLException
     */
    public static synchronized boolean insertUserInDB (String name, String surname, String email, Connection con) throws SQLException {
        
        if (con == null) throw new SQLException("[insertUserInDB] Connection Object is null");
        
        if (email.isEmpty()) return false;
        
        PreparedStatement pst = null;
        try {
            pst = con.prepareStatement("insert into \"PUBLIC\".\"USERS\" values(?,?,?)");
            pst.clearParameters();
            pst.setString(1, name);
            pst.setString(2, surname);
            pst.setString(3, email);
            pst.executeUpdate();
            pst.close();
            System.out.println("Row inserted");
            return true;
        } catch (SQLException e) {
            System.err.println("[insertUserInDB]  Error during the insert query");
            e.printStackTrace();
            return false;
        }
        finally {
            if (pst != null) {
                pst.close();    
            }
        }
    }
    
    /**
     * 
     * Method used to update the user table using the email as a condition.
     * 
     * @param name
     * @param surname
     * @param email
     * @param con
     * @throws SQLException
     */
    public static synchronized boolean updateUserInDB (String name, String surname, String email, Connection con) throws SQLException {
        
        if (con == null) throw new SQLException("[insertUserInDB] Connection Object is null");
        
        if (email.isEmpty()) return false;
        
        PreparedStatement preparedStatement = null;
        try {
            String updateTableSQL = "UPDATE \"PUBLIC\".\"USERS\" SET NAME = ?, SURNAME = ? WHERE EMAIL = ?";
            preparedStatement = con.prepareStatement(updateTableSQL);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, email);
            // execute insert SQL stetement
            preparedStatement .executeUpdate();
            return true;
            
        } catch (SQLException e) {
            System.err.println("[updateUserInDB]  Error during the update query");
            e.printStackTrace();
        }
        finally {
            if (preparedStatement != null) {
                preparedStatement.close();    
            }
        }
        return false;
    }    
    
    /**
     * 
     * Method used to get a User from the email.
     * If there isn't a User with the email passed as a input parameter the returned object will be null
     * 
     * @param email
     * @param con
     * @return
     * @throws SQLException
     */
    public static synchronized User findUserByEmail (String email, Connection con) throws SQLException  {
        
        if (con == null) throw new SQLException("[insertUserInDB] Connection Object is null");
        
        if (email.isEmpty()) return null;
        
        Statement stm = null;
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery("Select * from \"PUBLIC\".\"USERS\" WHERE EMAIL='" + email + "'");
            while (rs.next()) {
                User user = new User();
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setEmail(rs.getString("email"));
                
                return user;
            }
        } catch (SQLException e) {
            System.out.println("[findUserByEmail] Error during the select query");
            e.printStackTrace();
        }
        finally {
            if (stm != null) {
                stm.close();
            }
        }
        return null;

    }
    
    /**
     * 
     * Method used to get the list of all Users in table USERS
     * 
     * @param con
     * @return
     * @throws SQLException
     */
    public static synchronized List<User> getAllUsers (Connection con) throws SQLException {
        
        if (con == null) throw new SQLException("[getAllUsers] Connection Object is null");
        
        List <User> listOfUsers = new ArrayList<User>();
        Statement stm = null;
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery("Select * from \"PUBLIC\".\"USERS\"");
            while (rs.next()) {
                User user = new User();
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setEmail(rs.getString("email"));
 
                listOfUsers.add(user);
            }
            
            return listOfUsers;
            
        } catch (SQLException e) {
            System.err.println("[getAllUsers] Error during the select query");
            e.printStackTrace();
        }
        finally {
            if (stm != null) {
                stm.close();
            }
        }
        return null;
    }

}


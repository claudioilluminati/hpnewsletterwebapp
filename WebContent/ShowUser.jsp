<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Information</title>
</head>
<body>
	<h1>User data</h1>

	<table cellspacing="5" cellpadding="5" border="1">
		<tr>
			<td><b>Name</b></td>
			<td><b>Surname</b></td>
			<td><b>Email</b></td>
		</tr>
		<c:forEach items="${users}" var="user">

			<tr>
				<td>${user.name}</td>
				<td>${user.surname}</td>
				<td>${user.email}</td>
			</tr>

		</c:forEach>
	</table>

	<p>
		Click on Back button of your browser or on New User button <br /> to
		insert a new user!
	</p>

	<form action="index.jsp" method="post">
		<input type="submit" value="New User" />
	</form>

</body>
</html>
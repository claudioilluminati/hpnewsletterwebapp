<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert User Data</title>
<script language="Javascript">
	function emailcheck(theForm) {
		var theForm = /^(([^()[\]\\.,;:\s@\"]+(\.[^()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		if (!filter.test(str)) {
			alert("invalid Email id");
			return false;
		} else {
			return true;
		}
	}

	function ValidateEmail() {
		var emailID = document.frm.email

		if ((emailID.value == null) || (emailID.value == "")) {
			alert("Please Enter your Email Address");
			emailID.focus();
			return false;
		}
		if (emailcheck(emailID.value) == false) {
			emailID.value = "";
			emailID.focus();
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<h1>Insert User Data</h1>
	<p>
		Please fill data <br /> At the end click on submit.
	</p>

	<form name="frm" action="SimpleController" method="get"
		onSubmit="return ValidateEmail()">
		<table cellspacing="5" border="0">
			<tr>
				<td align="right">Name:</td>
				<td><input type="text" name="name" maxlength="45" /></td>
			</tr>
			<tr>
				<td align="right">Surname:</td>
				<td><input type="text" name="surname" maxlength="45" /></td>
			</tr>
			<tr>
				<td align="right">Email:</td>
				<td><input type="text" name="email" maxlength="45" /></td>
			</tr>
			<tr>
				<td><br> <input type="submit" name="Submit" value="Submit"></td>
			</tr>
		</table>
	</form>
	<form action="SimpleController" method="get">
		<table>
			<tr>
				<td><input type="submit" name="Submit" value="View Table"></td>
			</tr>
		</table>
	</form>

</body>
</html>